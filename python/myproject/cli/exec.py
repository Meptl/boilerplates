import click

@click.command()
@click.argument('path')
@click.option('--force', '-f', is_flag=True, help='Force flag.')
def exec(path, force):
    """Run a program."""
    print("Hello world")
